4 or 5 Inputs via command line arguments. 5th used to execute command only on a single machine


Input Descrition 
1  csv_file_having_ip_hostname: Header less 2 column input. 1) IP 2) Host name or any string to easily understand machine by Humans
2  Command: this command will be executed on machines
3  username: command will run using this username all restriction on the use will apply. I generally use root user to avoid permission issues.
4  password: Password should match for command to be executed.
5  [OPTIONAL] ip or hostname: if provided only row having this ip or hostname (as given in 1st csv) will execute command all other machine entries will skip this command. 
5th used to execute command only on a single machine and ignore other machine entries in input csv.. The 5th argument is meaning full only if it is among the input csv file. it is matched with csv. If a new ip or hostname is given all machines in csv will be ignored and commnd will not be executed on any machine.

OUTPUT: Whatever output the command gives is returned without any interpretation. special handling done to get disk usage of root dirve.



loops through each machine executes a command using new ssh connection on that machine.

The csv_file_having_ip_hostname, command, username, password are all command line arguments.

Sample bat file is provided 



If we add one more commadn line argument it is treated as ip or a hostname. still program will loop through all ips inteh input file. but now it will check if the ip or hostname matches teh current ip or hostname in the file. if it does then only the commandis executed. so effectively the command gest executed only on one machine which is provided and not on all machines.
